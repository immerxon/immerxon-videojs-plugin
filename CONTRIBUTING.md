# CONTRIBUTING

We welcome contributions from everyone!

## Getting Started

Make sure you have NodeJS 0.10 or higher and npm installed.

1. Fork this repository and clone your fork
2. Install dependencies: `npm install`
3. Run a development server: `npm start`

## Building

1. Make your changes
2. Test then with `npm test` (or `npm start` and test http://localhost:9999/test/ in your browser)
3. Build the code with `npm run build`
4. Commit your changes

### Making Changes

Refer to the [video.js plugin conventions][conventions] for more detail on best practices and tooling for video.js plugin authorship.

When you've made your changes, push your commit(s) to your fork and issue a pull request against the original repository.

### Running Tests

Testing is a crucial part of any software project. For all but the most trivial changes (typos, etc) test cases are expected. Tests are run in actual browsers using [Karma][karma].

- In all available and supported browsers: `npm test`
- In a specific browser: `npm run test:chrome`, `npm run test:firefox`, etc.
- While development server is running (`npm start`), navigate to [`http://localhost:9999/test/`][local]


[karma]: http://karma-runner.github.io/
[local]: http://localhost:9999/test/
[conventions]: https://github.com/videojs/generator-videojs-plugin/blob/master/docs/conventions.md
