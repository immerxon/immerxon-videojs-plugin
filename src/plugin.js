import videojs from 'video.js';
import {version as VERSION} from '../package.json';

// Cross-compatibility for Video.js 5 and 6.
const registerPlugin = videojs.registerPlugin || videojs.plugin;
const dom = videojs.dom || videojs;

// count how many scripts have been loaded
let loadedCount = 0;
const numberOfScripts = 2;

/**
 * Run Immerxon VideoJS Plugin
 *
 * @param {Object} options Some options to pass through to Immerxon UI
 */
function immerxonPlugin(options = {debug: true}) {
  // once the scripts are loaded, check to see if we are ready to execute
  const onScriptsLoaded = () => {
    loadedCount++;

    if (loadedCount >= numberOfScripts) {
      // ok now everything is loaded, so lets kick off the player
      require('initialize.coffee')(videojs.mergeOptions({player: this}, options));
    }
  };

  const host = options.debug ? 'http://localhost:8000' : '//create.immerxon.com';

  // create script/style nodes for Immerxon styles
  const script1 = dom.createEl('script', {
    src: `${host}/vendor.js`,
    async: 'true',
    onload: onScriptsLoaded
  });

  const script2 = dom.createEl('script', {
    src: `${host}/player.js`,
    async: 'true',
    onload: onScriptsLoaded
  });

  const stylesheet = dom.createEl('link', {
    rel: 'stylesheet',
    href: `${host}/player.css`
  });

  // and append them to the page
  dom.appendContent(this.el(), [script1, script2, stylesheet]);
}

// Register the plugin with video.js.
registerPlugin('immerxon', immerxonPlugin);

// Include the version number.
immerxonPlugin.VERSION = VERSION;

export default immerxonPlugin;
